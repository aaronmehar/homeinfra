<?php

# send a suitable iPXE script to a client
echo "#!ipxe\n";

switch ($_GET['mac']) {

  case '00:12:34:56:78:90':
    # boot pxelinux from this server
    echo "chain http://build.mehar.me/pxelinux.0\n";
    break;

  case '00:11:22:33:44:55':
    # boot from iSCSI
    echo "set initiator-iqn iqn.2007-08.com.example.initiator:initiator\n";
    # see http://ipxe.org/sanuri for the syntax
    echo "sanboot iscsi:san.example.com:6:3260:0:iqn.2007-08.me.mehar.san:sometarget\n";
    break;

  case '08:00:27:CE:79:D2':
    # boot menu
    echo "chain http://build.mehar.me/ipxe/menu.ipxe\n";
    break;

  case '00:1C:42:76:0E:C9':
    # boot menu 
    echo "chain http://boot.salstar.sk\n";
    break;

  default:
    # exit iPXE and let machine go on with BIOS boot sequence
    echo "exit\n";
    break;
}
?>
